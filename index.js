module.exports = {
    extends: ['airbnb-base',"plugin:vue/recommended"],
    rules: {
        indent: ['error', 4, { ignoreComments: true }],
        'no-use-before-define': 0,
        'comma-dangle': 0,
        'spaced-comment': 0,
        'no-new': 0,
        'no-multi-spaces': ['error', { ignoreEOLComments: true }],
        'no-plusplus': 0,
        'wrap-iife': 0,
        'no-underscore-dangle': 0,
        'one-var': 0,
        'max-len': ['error', { code: 120 }],
        'space-before-function-paren': ['error', 'never'],
        "no-param-reassign": ["error", { "props": false }],        
    },
    env: {
        browser: true,
        es6: true,
        jquery: true,
    },
    parser: 'babel-eslint',
    parserOptions: {
        sourceType: 'module',
        allowImportExportEverywhere: true
    }
};
