# ESLint Config herrlich media
Based on [airbnb-base](https://www.npmjs.com/package/eslint-config-airbnb-base)

## Änderungen
Bei Änderungen der Config immer das Tag updaten und die Version hochzählen

## Installation
    
```bash
$ npm install --save-dev eslint eslint-plugin-import git+ssh://bitbucket.org:herrlichmedia/herrliches-eslint.git
```

## Usage

Add ESLint config to your package.json:

```json
{
    "eslintConfig": {
        "extends": "./node_modules/herrliches-eslint/index.js"
    }
}
```
